package il.co.xsites.developertest.app;

import il.co.xsites.developertest.api.ClientDto;
import il.co.xsites.developertest.dao.ClientDao;
import il.co.xsites.developertest.dao.ClientSearchDao;
import il.co.xsites.developertest.entities.Client;
import il.co.xsites.developertest.exception.ClientNotFoundException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ClientService {
    private final ClientDao clientDao;
    private final ClientSearchDao clientSearchDao;
    private final Environment environment;

    public ClientService(ClientDao clientDao, ClientSearchDao clientSearchDao, Environment environment) {
        this.clientDao = clientDao;
        this.clientSearchDao = clientSearchDao;
        this.environment = environment;
    }

    public ClientDto find(long id) throws ClientNotFoundException {
        return dtoFromEntity(clientDao.find(id));
    }

    public void delete(long id) throws ClientNotFoundException {
        clientDao.delete(clientDao.find(id));
    }

    public ClientDto create(ClientDto data) {
        Client client = entityFromDto(data);
        clientDao.create(client);

        return dtoFromEntity(client);
    }

    public ClientDto update(long id, ClientDto data) throws ClientNotFoundException {
        Client client = clientDao.find(id);

        update(client, data);

        clientDao.update(client);

        return dtoFromEntity(client);
    }

    public List<ClientDto> getAll() {
        List<Client> all = clientDao.getAll();
        List<ClientDto> out = new ArrayList<>();
        for (Client c: all) {
            out.add(dtoFromEntity(c));
        }

        return out;
    }

    public List<ClientDto> search(String text) throws InterruptedException {
        List<Client> results;
        String searchProvider = environment.getProperty("search.provider");

        if ("lucene".equals(searchProvider)) {
            results = clientSearchDao.searchByFullName(text);
        } else {
            results = clientDao.search(text);
        }

        List<ClientDto> out = new ArrayList<>();
        for (Client c: results) {
            out.add(dtoFromEntity(c));
        }

        return out;
    }

    public List<ClientDto> searchLike(String text) throws InterruptedException {
        List<Client> results = clientDao.search(text);
        List<ClientDto> out = new ArrayList<>();
        for (Client c: results) {
            out.add(dtoFromEntity(c));
        }

        return out;
    }

    private void update(Client client, ClientDto dto) {
        client.setEmail(dto.getEmail());
        client.setFirstName(dto.getFirstName());
        client.setLastName(dto.getLastName());
        client.setBalance(dto.getBalance());
        client.setCurrency(dto.getCurrency());
    }

    public Client entityFromDto(ClientDto dto) {
        return new Client(
                dto.getEmail(), dto.getFirstName(), dto.getLastName(), dto.getBalance(), dto.getCurrency());
    }

    public ClientDto dtoFromEntity(Client client) {
        ClientDto dto = new ClientDto();
        dto.setId(client.getId());
        dto.setCreationTime(client.getCreationTime());
        dto.setEmail(client.getEmail());
        dto.setFirstName(client.getFirstName());
        dto.setLastName(client.getLastName());
        dto.setBalance(client.getBalance());
        dto.setCurrency(client.getCurrency());

        return dto;
    }
}
