package il.co.xsites.developertest.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TermVector;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Indexed
@Table(name = "client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, updatable = false)
    private Long id = null;

    @CreatedDate
    @Column(name = "creation_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;

    @Column
    private String email;

    @Field(termVector = TermVector.YES)
    @Column(name = "first_name")
    private String firstName;

    @Field(termVector = TermVector.YES)
    @Column(name = "last_name")
    private String lastName;

    @OneToOne(mappedBy = "client", orphanRemoval = true)
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @OnDelete(action = OnDeleteAction.CASCADE)
    @PrimaryKeyJoinColumn
    private ClientWallet wallet;

    public Client() {
    }

    public Client(
            String email,
            String firstName,
            String lastName,
            double balance,
            String currency
    ) {
        creationTime = new Date();
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        wallet = new ClientWallet(this, balance, currency);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBalance(double balance) {
        wallet.setBalance(balance);
    }

    public void setCurrency(String currency) {
        wallet.setCurrency(currency);
    }

    public double getBalance() {
        return wallet.getBalance();
    }

    public String getCurrency() {
        return wallet.getCurrency();
    }
}
