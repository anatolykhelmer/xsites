package il.co.xsites.developertest.entities;

import javax.persistence.*;

@Entity
@Table(name = "client_wallet")
public class ClientWallet {
    @Id
    @Column(name = "client_id")
    private Long clientId = null;

    @OneToOne
    @MapsId
    @JoinColumn(name = "client_id")
    private Client client;

    @Column
    private double balance;

    @Column
    private String currency;

    public ClientWallet() {
    }

    public ClientWallet(Client client, double balance, String currency) {
        this.client = client;
        this.balance = balance;
        this.currency = currency;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
