package il.co.xsites.developertest.api;

import il.co.xsites.developertest.app.ClientService;
import il.co.xsites.developertest.dao.ClientDao;
import il.co.xsites.developertest.exception.ClientNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/clients")
public class ClientsController {
    private final ClientDao clientDao;
    private final ClientService clientService;

    public ClientsController(ClientDao clientDao, ClientService clientService) {
        this.clientDao = clientDao;
        this.clientService = clientService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ClientDto>> getAll(@RequestParam(name = "q", required = false) String search) throws Exception {
        if (search != null) {
            //return ResponseEntity.ok(clientService.searchLike(search));
            return ResponseEntity.ok(clientService.search(search));
        }
        return ResponseEntity.ok(clientService.getAll());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> getById(@PathVariable("id") Long id) {
        try {
            ClientDto out = clientService.find(id);

            return ResponseEntity.ok(out);
        } catch (ClientNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
        try {
            clientService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (ClientNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> createNew(@Valid @RequestBody ClientDto request) {
        return ResponseEntity.ok(clientService.create(request));
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> update(@PathVariable("id") Long id, @Valid @RequestBody ClientDto request) {
        try {
            return ResponseEntity.ok(clientService.update(id, request));
        } catch (ClientNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
