package il.co.xsites.developertest.dao;

import il.co.xsites.developertest.entities.Client;
import il.co.xsites.developertest.exception.ClientNotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ClientDao {
    private final EntityManager em;

    public ClientDao(EntityManager em) {
        this.em = em;
    }

    public List<Client> search(String text) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Client> cq = cb.createQuery(Client.class);
        Root<Client> root = cq.from(Client.class);

        Predicate[] predicates = new Predicate[2];
        predicates[0] = cb.like(root.get("firstName"), "%" + text + "%");
        predicates[1] = cb.like(root.get("lastName"), "%" + text + "%");

        cq.where(cb.or(predicates));

        TypedQuery<Client> query = em.createQuery(cq);

        return query.getResultList();
    }

    public List<Client> getAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Client> cq = cb.createQuery(Client.class);
        Root<Client> root = cq.from(Client.class);
        cq.select(root);

        TypedQuery<Client> query = em.createQuery(cq);

        return query.getResultList();
    }

    public Client find(long id) throws ClientNotFoundException {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Client> cq = cb.createQuery(Client.class);
        Root<Client> root = cq.from(Client.class);

        cq.where(cb.equal(
                root.get("id"),
                id
        ));

        TypedQuery<Client> query = em.createQuery(cq);

        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            throw new ClientNotFoundException();
        }
    }

    @Transactional
    public void update(Client client) {
        em.persist(client);
    }

    @Transactional
    public void delete(Client client) {
        em.remove(client);
    }

    @Transactional
    public void create(Client client) {
        em.persist(client);
    }
}
