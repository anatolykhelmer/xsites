package il.co.xsites.developertest.dao;

import il.co.xsites.developertest.entities.Client;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class ClientSearchDao {
    private final EntityManager em;

    private boolean initiated = false;

    public ClientSearchDao(EntityManager em) {
        this.em = em;
    }

    private void init() throws InterruptedException {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        // TODO: this is dirty hack - change to something else
        fullTextEntityManager.createIndexer().startAndWait();
        initiated = true;
    }

    public List<Client> searchByFullName(String text) throws InterruptedException {
        if (!initiated) init();

        Query query = getQueryBuilder().simpleQueryString()
                .onFields("firstName", "lastName")
                .matching(text)
                .createQuery();

        return (List<Client>) getJpaQuery(query).getResultList();
    }

    private FullTextQuery getJpaQuery(org.apache.lucene.search.Query luceneQuery) {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);

        return fullTextEntityManager.createFullTextQuery(luceneQuery, Client.class);
    }

    private QueryBuilder getQueryBuilder() {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);

        return fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(Client.class)
                .get();
    }
}
