package il.co.xsites.developertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan({"il.co.xsites.developertest.entities"})
public class DevelopertestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevelopertestApplication.class, args);
	}
}
