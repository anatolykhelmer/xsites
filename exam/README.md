# Developer Test

**Please read the entire file before starting your test!**

## Test Goal

In this test, you will be given a bare-bones Spring Boot project in order to implement a basic CRUD on a single entity. There are no architectural guidelines (Aside from a few limitations we'll get into later), so you are free to write the project however you see fit.

For your convenience, you are provided with the complete [API doc](./assets/api_doc.html), a [Swagger definition](./assets/swagger.yml), and a [Postman Collection](./assets/postman_collection.json) of the required API.

The project makes use of a database seed which can be found at `resources/import.sql`. As such, you first must create two JPA entities, `Client` (With table name `client`) and `ClientWallet` (with table name `client_wallet`) which match the seed.

Schema diagram:

![erd](./assets/db_erd.png)

The `ClientWallet` entity should be related to the `Client` entity with a `OneToOne` relation, and all edits to fields on `ClientWallet` should be done via `Client`. Additionally, when a new `Client` is created, its `ClientWallet` should be automatically initialized.

When viewing clients via the endpoints, `ClientWallet` should be flattened. Meaning that the `balance` and `currency` fields will appear in the response like so:
```json
    {
        "id": 1,
        "creationTime": "2019-11-14T10:44:15.000+0000",
        "firstName": "Johne",
        "lastName": "Wilcox",
        "email": "wilcox.j@dihe.com.au",
        "currency": "USD",
        "balance": 256.0
    }
```
\- With currency and balance as part of the main object. **How you accomplish this is up to you.** 

Likewise, when editing or creating a `Client`, the object you will receive will be flattened, and must be un-flattened with its values entered into `Client` and `ClientWallet` respectively.

Once you have created the entities, you must create the endpoints outlined in the API documentation. 

## Limitations

In this test, **you may only use Hibernate Criteria Queries**.

Meaning you are not allowed to use SQL queries, JPQL/HQL queries, Spring CrudRepository/JpaRepository etc.

Additionally, you may not:
* Add/Remove Maven dependencies

Aside from the above, you are allowed to accomplish the task in whichever way you see fit.

## Summary

1. Create two entities - `Client`, `ClientWallet` (See schema diagram).
Once your create the entities, data will be populated automatically in the Database.

2. The entities should have a one-to-one relationship.

3. When a new `Client` is created, its `ClientWallet` should automatically be initialized.

4. All edits to fields on `ClientWallet` should be done via `Client`.

5. When viewing clients via the endpoints, `ClientWallet` should be flattened (See example).

6. Implement all the endpoints in the [Swagger definition](./assets/swagger.yml) (Paste into https://editor.swagger.io/).

